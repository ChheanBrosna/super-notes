import 'package:auto_route/auto_route.dart';
import 'package:super_notes/screens/dashboard/dashboard_tab.dart';
import 'package:super_notes/screens/screens.dart';
import 'package:super_notes/screens/start_up_screen.dart';

@MaterialAutoRouter(
  replaceInRouteName: 'Page,Route',
  routes: <AutoRoute>[
    AutoRoute(
      initial: true,
      page: StartUpScreen,
    ),
    AutoRoute(
      page: DashboardTab,
      name: 'DashboardRouter',
      children: [
        AutoRoute(name: 'HomeRouter', page: HomeScreen),
        AutoRoute(name: 'TodoListRouter', page: TodoListScreen),
        AutoRoute(name: 'CovidRouter', page: CovidScreen),
        AutoRoute(name: 'EmergencyRouter', page: EmergencyScreen),
        AutoRoute(name: 'MoreRouter', page: MoreScreen),
      ],
    ),
  ],
)
class $AppRouter {}
