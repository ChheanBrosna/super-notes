// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************
//
// ignore_for_file: type=lint

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:auto_route/auto_route.dart' as _i4;
import 'package:flutter/material.dart' as _i5;

import '../screens/dashboard/dashboard_tab.dart' as _i2;
import '../screens/screens.dart' as _i3;
import '../screens/start_up_screen.dart' as _i1;

class AppRouter extends _i4.RootStackRouter {
  AppRouter([_i5.GlobalKey<_i5.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i4.PageFactory> pagesMap = {
    StartUpScreen.name: (routeData) {
      return _i4.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i1.StartUpScreen());
    },
    DashboardRouter.name: (routeData) {
      return _i4.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i2.DashboardTab());
    },
    HomeRouter.name: (routeData) {
      return _i4.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i3.HomeScreen());
    },
    TodoListRouter.name: (routeData) {
      return _i4.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i3.TodoListScreen());
    },
    CovidRouter.name: (routeData) {
      return _i4.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i3.CovidScreen());
    },
    EmergencyRouter.name: (routeData) {
      return _i4.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i3.EmergencyScreen());
    },
    MoreRouter.name: (routeData) {
      return _i4.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i3.MoreScreen());
    }
  };

  @override
  List<_i4.RouteConfig> get routes => [
        _i4.RouteConfig(StartUpScreen.name, path: '/'),
        _i4.RouteConfig(DashboardRouter.name,
            path: '/dashboard-tab',
            children: [
              _i4.RouteConfig(HomeRouter.name,
                  path: 'home-screen', parent: DashboardRouter.name),
              _i4.RouteConfig(TodoListRouter.name,
                  path: 'todo-list-screen', parent: DashboardRouter.name),
              _i4.RouteConfig(CovidRouter.name,
                  path: 'covid-screen', parent: DashboardRouter.name),
              _i4.RouteConfig(EmergencyRouter.name,
                  path: 'emergency-screen', parent: DashboardRouter.name),
              _i4.RouteConfig(MoreRouter.name,
                  path: 'more-screen', parent: DashboardRouter.name)
            ])
      ];
}

/// generated route for
/// [_i1.StartUpScreen]
class StartUpScreen extends _i4.PageRouteInfo<void> {
  const StartUpScreen() : super(StartUpScreen.name, path: '/');

  static const String name = 'StartUpScreen';
}

/// generated route for
/// [_i2.DashboardTab]
class DashboardRouter extends _i4.PageRouteInfo<void> {
  const DashboardRouter({List<_i4.PageRouteInfo>? children})
      : super(DashboardRouter.name,
            path: '/dashboard-tab', initialChildren: children);

  static const String name = 'DashboardRouter';
}

/// generated route for
/// [_i3.HomeScreen]
class HomeRouter extends _i4.PageRouteInfo<void> {
  const HomeRouter() : super(HomeRouter.name, path: 'home-screen');

  static const String name = 'HomeRouter';
}

/// generated route for
/// [_i3.TodoListScreen]
class TodoListRouter extends _i4.PageRouteInfo<void> {
  const TodoListRouter() : super(TodoListRouter.name, path: 'todo-list-screen');

  static const String name = 'TodoListRouter';
}

/// generated route for
/// [_i3.CovidScreen]
class CovidRouter extends _i4.PageRouteInfo<void> {
  const CovidRouter() : super(CovidRouter.name, path: 'covid-screen');

  static const String name = 'CovidRouter';
}

/// generated route for
/// [_i3.EmergencyScreen]
class EmergencyRouter extends _i4.PageRouteInfo<void> {
  const EmergencyRouter()
      : super(EmergencyRouter.name, path: 'emergency-screen');

  static const String name = 'EmergencyRouter';
}

/// generated route for
/// [_i3.MoreScreen]
class MoreRouter extends _i4.PageRouteInfo<void> {
  const MoreRouter() : super(MoreRouter.name, path: 'more-screen');

  static const String name = 'MoreRouter';
}
