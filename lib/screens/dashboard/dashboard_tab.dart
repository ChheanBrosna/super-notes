import 'package:auto_route/auto_route.dart';
import 'package:super_notes/singleton/signleton.dart';
import 'package:super_notes/themes/app_theme.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:super_notes/app_router/app_router.gr.dart';
import 'package:super_notes/widgets/widgets.dart';

class DashboardTab extends HookWidget {
  const DashboardTab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    void _searchHandleTap() {}

    return AutoTabsRouter(
      routes: const [
        HomeRouter(),
        TodoListRouter(),
        CovidRouter(),
        EmergencyRouter(),
        MoreRouter(),
      ],
      builder: (context, child, animation) {
        final tabsRouter = AutoTabsRouter.of(context);

        AppBar _dynamicAppBar() {
          if (tabsRouter.activeIndex == 0) {
            return AppBar(
              title: Text(tr('screen.HOME')),
              actions: [
                Padding(
                  padding:
                      EdgeInsets.only(right: Metrics.instance.defaultPadding),
                  child: Row(
                    children: [
                      InkWell(
                        onTap: () {},
                        child: SvgIconWidget(
                          'assets/svgs/filter.svg',
                          size: Metrics.instance.iconSize,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            );
          } else if (tabsRouter.activeIndex == 1) {
            return AppBar(
              title: Text(tr('screen.TODOLIST')),
            );
          } else if (tabsRouter.activeIndex == 2) {
            return AppBar(
              title: Text(tr('screen.COVID')),
            );
          } else if (tabsRouter.activeIndex == 3) {
            return AppBar(
              title: Text(tr('screen.EMERGENCY')),
            );
          } else {
            return AppBar(
              title: Text(tr('screen.MORE')),
            );
          }
        }

        return Scaffold(
          appBar: _dynamicAppBar(),
          body: FadeTransition(
            opacity: animation,
            child: child,
          ),
          bottomNavigationBar: BottomNavigationBar(
            backgroundColor: AppColors.white,
            unselectedItemColor: Colors.grey,
            showSelectedLabels: true,
            elevation: 10,
            showUnselectedLabels: true,
            selectedItemColor: AppColors.primary,
            currentIndex: tabsRouter.activeIndex,
            type: BottomNavigationBarType.fixed,
            onTap: (index) {
              tabsRouter.setActiveIndex(index);
            },
            items: [
              BottomNavigationBarItem(
                label: tr('screen.home'),
                icon: const Icon(
                  Icons.home_outlined,
                  color: AppColors.iconInActive,
                ),
                activeIcon: const Icon(
                  Icons.home_outlined,
                  color: AppColors.primary,
                ),
              ),
              BottomNavigationBarItem(
                label: tr('screen.todoList'),
                icon: const Icon(
                  Icons.task_outlined,
                  color: AppColors.iconInActive,
                ),
                activeIcon: const Icon(
                  Icons.task_outlined,
                  color: AppColors.primary,
                ),
              ),
              BottomNavigationBarItem(
                label: tr('screen.covid'),
                icon: const Icon(
                  Icons.coronavirus_outlined,
                  color: AppColors.iconInActive,
                ),
                activeIcon: const Icon(
                  Icons.coronavirus_outlined,
                  color: AppColors.primary,
                ),
              ),
              BottomNavigationBarItem(
                label: tr('screen.emergency'),
                icon: const Icon(
                  Icons.phone_outlined,
                  color: AppColors.iconInActive,
                ),
                activeIcon: const Icon(
                  Icons.phone_outlined,
                  color: AppColors.primary,
                ),
              ),
              BottomNavigationBarItem(
                label: tr('screen.more'),
                icon: const Icon(
                  Icons.more_vert,
                  color: AppColors.iconInActive,
                ),
                activeIcon: const Icon(
                  Icons.more_vert,
                  color: AppColors.primary,
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
