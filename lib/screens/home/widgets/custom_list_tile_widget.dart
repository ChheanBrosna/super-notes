import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:super_notes/singleton/signleton.dart';
import 'package:super_notes/themes/app_theme.dart';
import 'package:super_notes/widgets/widgets.dart';

class CustomListTileWidget extends StatelessWidget {
  const CustomListTileWidget({
    Key? key,
    required this.title,
    this.subTitle,
    this.trillingText,
    this.circleAvatarText,
    this.index,
    this.length,
  }) : super(key: key);

  final String title;
  final String? subTitle;
  final String? trillingText;
  final String? circleAvatarText;
  final int? index;
  final int? length;

  @override
  Widget build(BuildContext context) {
    return Slidable(
      endActionPane: ActionPane(
        motion: const ScrollMotion(),
        children: [
          SlidableAction(
            onPressed: (context) {},
            backgroundColor: Colors.red,
            foregroundColor: Colors.white,
            icon: Icons.delete,
            label: tr('button.DELETE'),
          ),
          SlidableAction(
            onPressed: (context) {},
            backgroundColor: const Color(0xFF0392CF),
            foregroundColor: Colors.white,
            icon: Icons.update,
            label: tr('button.UPDATE'),
          ),
        ],
      ),
      child: Padding(
        padding: EdgeInsets.only(
          top: (index ?? 0) != 0 ? Metrics.instance.medium : 0,
        ),
        child: Column(
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CircleAvatar(
                  backgroundColor: AppColors.primary,
                  radius: 30,
                  child: MediumTextWidget(
                    text: circleAvatarText ?? '',
                    color: AppColors.white,
                  ),
                ),
                SizedBox(width: Metrics.instance.medium),
                Padding(
                  padding: EdgeInsets.only(top: Metrics.instance.small),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      MediumTextWidget(text: title),
                      SizedBox(height: Metrics.instance.small),
                      SmallTextWidget(text: subTitle ?? ''),
                    ],
                  ),
                ),
                const Expanded(child: SizedBox()),
                Padding(
                  padding: EdgeInsets.only(top: Metrics.instance.small),
                  child: RegularTextWidget(text: trillingText ?? ''),
                ),
              ],
            ),
            const Divider(
              thickness: .3,
              indent: 73,
              height: 0,
            ),
            if ((index ?? 0) == (length ?? 0) - 1)
              SizedBox(height: Metrics.instance.defaultPadding),
          ],
        ),
      ),
    );
  }
}
