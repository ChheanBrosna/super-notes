import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:super_notes/singleton/signleton.dart';
import 'package:super_notes/themes/app_theme.dart';
import 'package:super_notes/widgets/widgets.dart';

class DashboardWidget extends StatelessWidget {
  const DashboardWidget({Key? key, required this.expenses}) : super(key: key);

  final String expenses;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        AssetsImageWidget(
          path: 'assets/images/dashboard.png',
          height: Metrics.instance.dashbaordImageHeight,
        ),
        Positioned(
          left: Metrics.instance.large,
          top: Metrics.instance.large,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              TitleTextWidget(
                text: tr('screen.totalExpensesToday'),
                color: AppColors.white,
              ),
              SizedBox(height: Metrics.instance.medium),
              TitleTextWidget(
                text: expenses,
                color: AppColors.white,
              ),
            ],
          ),
        ),
      ],
    );
  }
}
