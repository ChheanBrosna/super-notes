import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:super_notes/screens/home/widgets/widgets.dart';
import 'package:super_notes/singleton/signleton.dart';
import 'package:super_notes/widgets/widgets.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.only(
          top: Metrics.instance.defaultPadding,
          left: Metrics.instance.defaultPadding,
          right: Metrics.instance.defaultPadding,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const DashboardWidget(expenses: '\$ 32.54'),
            SizedBox(height: Metrics.instance.medium),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                TitleTextWidget(text: tr('screen.allExpenses')),
                Row(
                  children: [
                    InkWell(
                      onTap: () {},
                      child: SvgIconWidget(
                        'assets/svgs/search.svg',
                        size: Metrics.instance.iconSize,
                      ),
                    ),
                    SizedBox(width: Metrics.instance.large),
                    InkWell(
                      onTap: () {},
                      child: SvgIconWidget(
                        'assets/svgs/add.svg',
                        size: Metrics.instance.iconSize,
                      ),
                    ),
                  ],
                ),
              ],
            ),
            SizedBox(height: Metrics.instance.medium),
            Expanded(
              child: ListView.builder(
                itemCount: 10,
                itemBuilder: (context, index) {
                  return CustomListTileWidget(
                    title: 'Go to shopping',
                    subTitle: '29 Aug 2022',
                    trillingText: '\$34.53',
                    circleAvatarText: 'M',
                    index: index,
                    length: 10,
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
