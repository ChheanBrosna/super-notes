import 'package:super_notes/app_router/app_router.gr.dart';
import 'package:flutter/material.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

class StartUpScreen extends HookWidget {
  const StartUpScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    useEffect(() {
      context.replaceRoute(const DashboardRouter());

      return () {};
    }, const []);
    return const Scaffold(
      body: Center(),
    );
  }
}
