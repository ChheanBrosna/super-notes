import 'package:super_notes/themes/material_color_generator.dart';
import 'package:flutter/material.dart';

class AppColors {
  static const white = Color(0xffffffff);
  static const primary = Color(0xFF1FB382);
  static const secondary = Color.fromARGB(255, 121, 131, 217);
  static const darkBlue = Color(0xff0F253F);
  static const lightGrey = Color(0xffCCD6DD);
  static const red = Color.fromARGB(255, 235, 29, 29);

  static const scaffoldBackground = Color(0xffEFEFF4);
  static const iconInActive = Color(0xff6c7989);
}

class AppTheme {
  const AppTheme._();

  static ThemeData define() {
    return ThemeData(
      primaryColor: AppColors.primary,
      primarySwatch: MaterialColorGenerator.generateMaterialColor(
        AppColors.primary,
      ),
      scaffoldBackgroundColor: AppColors.white,
      appBarTheme: const AppBarTheme(
        backgroundColor: AppColors.white,
        titleTextStyle: TextStyle(
          color: Colors.black,
          fontWeight: FontWeight.bold,
          fontSize: 17,
        ),
        iconTheme: IconThemeData(color: Colors.black),
        centerTitle: false,
        elevation: .5,
      ),
      textTheme: const TextTheme(
        headline1: TextStyle(
          color: Colors.black,
          fontSize: 20.0,
          fontWeight: FontWeight.bold,
        ),
        headline2: TextStyle(
          color: Colors.black,
          fontSize: 17.0,
          fontWeight: FontWeight.w700,
        ),
        bodyText1: TextStyle(
          color: Colors.black,
          fontSize: 15,
        ),
        bodyText2: TextStyle(
          color: AppColors.darkBlue,
          fontSize: 13,
        ),
      ),
      inputDecorationTheme: const InputDecorationTheme(
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: AppColors.lightGrey),
        ),
      ),
      elevatedButtonTheme: ElevatedButtonThemeData(
        style: ButtonStyle(
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
          ),
        ),
      ),
      dividerColor: AppColors.primary,

      // remove shadow color when clicked
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
    );
  }
}
