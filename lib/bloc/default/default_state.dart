part of 'default_bloc.dart';

@freezed
class DefaultState with _$DefaultState {
  const factory DefaultState.initial() = _Initial;
  const factory DefaultState.loadInProgress() = _LoadInProgress;
}