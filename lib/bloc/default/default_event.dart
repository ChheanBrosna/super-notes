part of 'default_bloc.dart';

@freezed
class DefaultEvent with _$DefaultEvent {
  const factory DefaultEvent.requestDefault() = _RequestDefault;
}
