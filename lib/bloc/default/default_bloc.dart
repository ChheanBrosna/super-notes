import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'default_state.dart';
part 'default_event.dart';
part 'default_bloc.freezed.dart';

class DefaultBloc extends Bloc<DefaultEvent, DefaultState> {
  DefaultBloc() : super(const DefaultState.initial()) {
    on<_RequestDefault>(_onRequestDefault);
  }
  void _onRequestDefault(_RequestDefault event, Emitter<DefaultState> emit) {}
}
